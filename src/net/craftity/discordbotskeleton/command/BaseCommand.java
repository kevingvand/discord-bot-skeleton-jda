package net.craftity.discordbotskeleton.command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public abstract class BaseCommand {

    public String Name;

    public BaseCommand(String name) {
        this.Name = name;
    }

    public abstract boolean execute(String[] args, MessageReceivedEvent event);

}
