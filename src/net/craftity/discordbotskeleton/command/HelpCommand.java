package net.craftity.discordbotskeleton.command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class HelpCommand extends BaseCommand {

    public HelpCommand() {
        super("help");
    }

    public boolean execute(String[] args, MessageReceivedEvent event) {
        event.getTextChannel().sendMessage("Hello There!").queue();
        return true;
    }
}
