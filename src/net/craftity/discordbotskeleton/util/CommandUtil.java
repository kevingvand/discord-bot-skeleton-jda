package net.craftity.discordbotskeleton.util;

import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.TextChannel;

public class CommandUtil {

    public static void SendMessage(TextChannel channel, String message) {
        channel.sendMessage(message).queue();
    }

    public static void SendMessage(MessageChannel channel, String message) {
        channel.sendMessage(message).queue();
    }
}
