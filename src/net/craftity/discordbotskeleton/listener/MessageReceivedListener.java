package net.craftity.discordbotskeleton.listener;

import net.craftity.discordbotskeleton.init.Commands;
import net.craftity.discordbotskeleton.reference.BotReference;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.Arrays;

public class MessageReceivedListener extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if(event.getMessage().getContent().startsWith(BotReference.EXECUTOR) && !event.getMessage().getAuthor().getId().equals(event.getJDA().getSelfUser().getId())) {
            String[] commandArray = parseCommand((event.getMessage().getContent()));
            String[] arguments;


            if(commandArray.length > 1) {
                arguments = Arrays.copyOfRange(commandArray, 1, commandArray.length);
            } else arguments = null;

            Commands.Execute(commandArray[0], arguments, event);
        }
    }

    private String[] parseCommand(String message) {
        message = message.replaceFirst(BotReference.EXECUTOR, "");
        return message.split(" ");
    }
}
