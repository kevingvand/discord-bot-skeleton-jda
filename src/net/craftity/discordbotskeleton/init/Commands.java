package net.craftity.discordbotskeleton.init;

import net.craftity.discordbotskeleton.command.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.ArrayList;

public class Commands {
    public static ArrayList<BaseCommand> Commands = new ArrayList<BaseCommand>();

    public static void init() {
        Commands.add(new HelpCommand());
    }

    public static boolean Execute(String name, String[] args, MessageReceivedEvent event) {
        for(BaseCommand command : Commands) {
            if(command.Name.equalsIgnoreCase(name)) {
                return command.execute(args, event);
            }
        }

        return false;
    }
}
