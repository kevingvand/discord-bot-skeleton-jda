package net.craftity.discordbotskeleton;

import net.craftity.discordbotskeleton.init.Commands;
import net.craftity.discordbotskeleton.listener.MessageReceivedListener;
import net.craftity.discordbotskeleton.reference.BotReference;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

public class Main {

    public static void main(String[] args) {
        try {

            JDA jdaBuilder = new JDABuilder(AccountType.BOT)
                    .addEventListener(new MessageReceivedListener())
                    .setToken(BotReference.TOKEN)
                    .buildBlocking();

            jdaBuilder.setAutoReconnect(true);

            Commands.init();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
